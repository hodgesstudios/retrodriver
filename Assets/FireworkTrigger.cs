﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworkTrigger : MonoBehaviour {

    public ParticleSystem[] fireworks;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < fireworks.Length; i++)
            {
                fireworks[i].Play(false);
            }
        }
    }
}
