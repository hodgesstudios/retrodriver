﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdMember : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Animator>().SetInteger("Cheer", Random.Range(1,4));
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Player") {
			GetComponent<Animator>().enabled = false;
			GetComponent<Rigidbody>().isKinematic = true;
			GetComponent<Rigidbody>().useGravity = false;
			GetComponent<Collider>().enabled = false;
		}
	}

}
