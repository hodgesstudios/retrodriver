﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TyreRotationFix : MonoBehaviour {

    private void LateUpdate()
    {
        transform.rotation = Quaternion.LookRotation(transform.parent.parent.forward);
    }
}
