﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The CarController handles the movement of the car for the Introduction to Unity presentation
/// </summary>
/// <author email="nathan@scrapgames.com.au">Nathan Sheppard</author>
/// <author email="info@hodgesstudios.com.au">J Christian Hodges</author>
public class CarController : MonoBehaviour
{

    [Header("Controls")]
    [Tooltip("The speed coefficient for the vehicle; essentially defines a max speed")]
    public float speed = 85f;
    [Tooltip("The threshold defines a 'deadzone' for steering, so that turning is not too sensitive")]
    public float threshold = 0.2f;
    [Tooltip("The steering threshold ensures the vehicle can only turn when it is moving more than a certain speed")]
    public float steerThreshold = 1.4f;
    [Tooltip("Scaling coefficient for the car's velocity used during steering rotation calculation")]
    public float driftFactor = 0.034f;
    public GameObject particleSpark;
    public List<CarWheel> wheels = new List<CarWheel>();

    // accessors
    private Rigidbody rb;
    private Vector3 force;

    // this isnt necessary if we take out the steering limiter
    //public float maxSteerVelocity;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // reset the force each frame
        force = Vector3.zero;
        // calculate steering and store into force
        Steer();
        // calculate acceleration and store into force
        Accelerate();
        // move the vehicle based on the force
        Move();
        Debug.DrawRay(transform.position, rb.velocity, Color.red);
    }

    private void Steer()
    {
        // grab the horizontal input
        float horizontal = Input.GetAxis("Horizontal");

        float dot = Vector3.Dot(transform.forward, rb.velocity);

        // if the vehicle is moving faster than the steering threshold
        if (rb.velocity.sqrMagnitude > steerThreshold)
        {
            // if our input is outside our deadzone
            if (Mathf.Abs(horizontal) > threshold)
            {
                // turning is in the right direction, by the input, and adjusted with the velocity
                Vector3 desired = transform.right * horizontal * rb.velocity.magnitude * ((dot < 0) ? -1 : 1);
                // CH thinks it feels better without the velocity limiting
                // if (desired.magnitude > maxSteerVelocity) desired = desired.normalized * maxSteerVelocity;
                force += (desired - rb.velocity);
            }
        }
        // set the rotation to look forward with a slight offset according to the 'force'
        transform.rotation = Quaternion.LookRotation(transform.forward + force.normalized * driftFactor);
    }

    private void Accelerate()
    {
        // grab input
        float veritcal = Input.GetAxis("Vertical");
        // ensure is in input threshold
        if (Mathf.Abs(veritcal) > threshold)
        {
            // add the acceleration to force
            force += transform.forward * veritcal * speed;
        }
    }

    private void Move()
    {
        // ensure the car is grounded using the "isGrounded" method
        if (IsGrounded())
            // add the force
            rb.AddForce(force, ForceMode.Acceleration);
    }

    // if at least one wheel is on the ground, return true
    private bool IsGrounded()
    {
        foreach (CarWheel wheel in wheels)
        {
            if (wheel.isGrounded)
            {
                return true;
            }
        }
        return false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Collision Prop") ||
            collision.gameObject.CompareTag("Building"))
        {
            particleSpark.transform.position = collision.contacts[0].point;
            particleSpark.transform.LookAt(-collision.contacts[0].normal * 2);
            particleSpark.GetComponent<ParticleSystem>().Play();
        }
    }
}
