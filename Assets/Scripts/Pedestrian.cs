﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pedestrian : MonoBehaviour {

    [SerializeField]
    Transform destination;

    NavMeshAgent navMeshAgent;

	// Use this for initialization
	void Start ()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

        SetDestination();
	}

    private void SetDestination()
    {
        navMeshAgent.SetDestination(destination.position);
    }

   
}
