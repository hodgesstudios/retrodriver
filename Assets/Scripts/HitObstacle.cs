﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class HitObstacle : MonoBehaviour {

    public float explosionForce;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            GetComponent<Rigidbody>().AddExplosionForce(explosionForce, collision.contacts[0].point, 2f);
        }
    }
}
   